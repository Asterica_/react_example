window.ee = new EventEmitter();

function cl(arg) {
    console.log(arg);
}

var WACounter = 0;


function htmlDecode(input){
  var e = document.createElement('div');
  e.innerHTML = input;
  return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

function escapeHtml(text) {
    return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/\n/g, "\\\\n")
        .replace(/\t/g, "\\\\t")
        .replace(/"/g, "&quot;");
}

class TextInput extends React.Component
{
	constructor(props){
		super(props);

		this.state = {
			val: this.props.val || ""
		}
	}

	onValueChanged(e){
		this.setState({val: e.target.value})
	}

    render(){
    	var val = this.state.val;
    	var addClass = this.props.additionalClasses;
    	
    	if (typeof(addClass) == "object")
    		addClass = addClass.join(" ");
        return (
            <div className="TextInput">
                <input 
                	className={"admin-input " + addClass}
                	type="text" 
                	onChange={(e) => this.onValueChanged(e)}
                	value={val}
                />
            </div>
        )
    }
}


class FileInput extends React.Component
{
    constructor(props){
        super(props);

        this.state = {
            val: this.props.val || ""
        }
    }

    onValueChanged(e){
        this.setState({val: e.target.value})
    }

    render(){
        var val = this.state.val;
        var addClass = this.props.additionalClasses;
        
        if (typeof(addClass) == "object")
            addClass = addClass.join(" ");
        return (
            <div className="FileInput">
                <input 
                    className={"admin-input " + addClass}
                    type="file" 
                    onChange={(e) => this.onValueChanged(e)}
                    value={val}
                />
            </div>
        )
    }
}



class Selecter extends React.Component
{
	constructor(props){
		super(props);

		this.state = {
			val: this.props.val
		}
	}

	onValueChanged(e){
		this.setState({val: e.target.value});
	}


    render(){
    	var val = this.state.val;
    	var optionsData = this.props.optData;
    	var options = optionsData.map(function(item, index){
    		return (
    			<option className="testSelect" key={index} value={item.key}>{item.text}</option>
    		)
    	});
        return (
            <div className="Selecter">
                <select className="admin-input__label" onChange={(e)=>this.onValueChanged(e)} value={val}>
                    {options}
                </select>
            </div>

        )
    }
}

class RefSelecter extends React.Component
{
    constructor(props){
        super(props);

    }




    render(){

        var optionsData = this.props.optData;
        var options = optionsData.map(function(item, index){
            return (
                <option className="testSelect" key={index} value={item.key}>{item.text}</option>
            )
        });
        return (
            <div className="Selecter">
                <select className="admin-input__label" defaultValue={options[0].key} ref={this.props.link}>
                    {options}
                </select>
            </div>

        )
    }
}


class BlockType extends React.Component
{
	constructor(props){
		super(props);

		this.state = {
			val: this.props.val
		}
	}

	onValueChanged(event){
		this.setState({val: event.target.value});
	}

    render(){
    	var val = this.state.val;

    	let selectData = [
    		{key: "common" , text: "Обычный"},
    		{key: "executable" , text: "Исполняемый"}
    	];

        return (
            <div className="BlockType">  
                <Selecter optData={selectData} val={val} />
            </div>
        )
    }
}


class CheckBox extends React.Component
{
    constructor(props){
        super(props);

        this.state = {
            checked: this.props.checked || false
        }
    }

    onToggle(e){
        this.setState({checked: e.target.checked});
    }

    render(){
        var value = this.state.checked,
            addClass = this.props.additionalClasses;


        return (
            <div className="CheckBox">
                <input 
                    className={addClass}
                    type="checkbox"
                    checked={value}
                    onChange={(e) => this.onToggle(e)}
                />
            </div>
        );
    }
}


class Area extends React.Component
{

    constructor(props){
        super(props);

        this.state = {
            val: this.props.val || ""
        }
    }

    onValueChanged(event){
        this.setState({val: event.target.value});
    }

    render(){
        var val = this.state.val,
            addClass = this.state.additionalClasses;
        return (
            <div className="Area">
                <textarea className={addClass + " admin-input"} value={val} onChange={(e) => this.onValueChanged(e)}  />
            </div>
        )
    }
}

class WysiwygArea extends React.Component
{   

    constructor(props){
        super(props);

        this.state = {
            val: this.props.val || "",
            WAID: WACounter++
        }

    }

     componentDidMount(){
        // console.log($(this.selfRef));
        var textarea = $(this.selfRef);
        var heightp = textarea.parent().css('height');
        var editor = CKEDITOR.replace(textarea.attr( 'name' ) ,{height: heightp,filebrowserBrowseUrl : '/content/javascript/ckeditor/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl : '/content/javascript/ckeditor/ckfinder/ckfinder.html?Type=Images',
        filebrowserFlashBrowseUrl : '/content/javascript/ckeditor/ckfinder/ckfinder.html?Type=Flash',
        filebrowserUploadUrl : '/content/javascript/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl : '/content/javascript/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl : '/content/javascript/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'});
    }
    

    onValueChanged(event){
        this.setState({val: event.target.value});
    }

    render(){
        // console.log()
        var val = this.state.val,
            id = this.props.bid,
            uid = this.props.buid;
        var waid = this.state.WAID;

        return (
            <div className="WysiwygArea">
                <textarea className="admin-input CKEDITORTEXT" name={"wysiwyg" + waid} value={htmlDecode(val)} onChange={(e) => this.onValueChanged(e)}  ref = {c => this.selfRef = c} />
            </div>
        )
    }
}




class RedButton extends React.Component
{
    render(){
        var addClass = this.props.additionalClasses || "",
            text = this.props.text;
        return (
            <div className="RedButton red_bt add-block__btn">
                <button className={addClass + " red_bt"} onClick={() => this.props.callback()}>{text}</button>
            </div>
        )
    }
}


class DeleteElement extends React.Component
{   
    constructor(props) {
        super(props);
        //this.onBtnClickHandler = this.onBtnClickHandler.bind(this);
    }

    onBtnClickHandler(){
        // console.log(this.props.eKey);
        window.ee.emit("Block.delete", {
            eKey: this.props.eKey,
            isFresh: this.props.isFresh
        });
    }

    render(){
        return (
            <div className="DeleteElement denyBlock">
                <button onClick={() => this.onBtnClickHandler()} ></button>
            </div>
        )
    }
};

class Line extends React.Component
{
    constructor(props) {
        super(props);
    

        this.state = {
            itemOuter: this.props.data
        }

    }

    render() {
        var bid = this.props.bid,
            buid = this.props.buid;

        var itemOuter = this.state.itemOuter;
        var list = this.props.repeatList.group.map(function (item, index) {
            item.name = item.name || item.title;
            item.title = "";

            if (itemOuter.val[index] != undefined && itemOuter.val[index].value != null && itemOuter.val[index].value.length > 0) {
                item.value = itemOuter.val[index].value;
            } else {
                item.value = "";
            }
            // cl(itemOuter.id + "_" + item.id);
            return (
                <Variable key={item.id} data={item} bid={bid} buid={buid} />
            )
        });

        return (
            <div className="Line">
                {list}
            </div>
        )
    }

}

class Repeater extends React.Component
{
    constructor(props) {
        super(props);
        var values;
        // cl(this.props.data);

        if (this.props.data.length != 0) {
            values = this.props.data.map(function(item, index){
             
                return {
                    id: index,
                    val: item
                }
            });
        } else {
            values = [{id: 0, val: ""}];
        }
        // cl("fire");
        this.state = {
            values: values,
            nextID: values.length
        }
    }

    onAddButtonClick(){
        var newObjId = this.state.nextID;
        var values = [...this.state.values, {id: newObjId, val: ""}];
        this.setState({values:values, nextID: ++newObjId });
    }

    render(){
        var bid = this.props.bid,
            buid = this.props.buid;
        var self = this;
        // cl(self.state.values);
        var variables = self.state.values.map(function(itemOuter){

            return (
                <Line key={itemOuter.id} data={itemOuter} repeatList={self.props.repeatList} bid={bid} buid={buid} />
            )
        });


        return (
            <div className="Repeater">
                {variables} 
                <RedButton text={"..."} callback={() => this.onAddButtonClick()} />
            </div>
        )
    }
}

class Group extends React.Component
{
    constructor(props) {
        super(props);

        this.state = {
            groupList: this.props.groupList || [],
            values: this.props.data || []
        }
    }

    render(){
        var bid = this.props.bid,
            buid = this.props.buid,
            values = this.state.values;
        
        

        var group = this.state.groupList.map(function(item, index){
            // console.log(values.value[index].value);
        //    cl(item);
            item.name = item.title;
            item.title = "";
            if (values.value[index] != undefined && values.value[index].value.length > 0) {
                item.value = values.value[index].value;
            } else {
                item.value = "";
                
            }
            // cl(item.value);
            return(
                <Variable key={item.id} data={item} bid={bid} buid={buid} />
            )
        });


        return (
            <div className="Group">
                <input type="hidden" className="var_type" value={values.type} />
                <input type="hidden" className="var_id" value={values.id} />
                {group} 
            </div>
        )
    }
}

class Variable extends React.Component
{
    constructor(props) {
        super(props);
        
        this.state = {
            variable: this.props.data || []
        }
    }

    render(){
        var outerObj = this.state.variable,
            bid = this.props.bid,
            buid = this.props.buid
        // if (outerObj.type == "repeater") 
            // cl(outerObj.value);
        return (
            <div className="Variable var_region">
                <span className="var_heading">Переменная {outerObj.name}</span>
                <input type="hidden" className="var_type" value={outerObj.type} />
                <input type="hidden" className="var_id" value={outerObj.id} />

                { ( outerObj.type == "text" || outerObj.type == "code") &&
                    <Area val={htmlDecode(outerObj.value)}/>
                }

                { ( outerObj.type == "wysiwyg") &&
                    <WysiwygArea val={outerObj.value} bid={bid} buid={buid} />
                }

                { ( outerObj.type == "repeater") &&
                    <Repeater repeatList={outerObj.data} data={outerObj.value} bid={bid} buid={buid}/>
                }

                { ( outerObj.type == "file") &&
                    <div></div>
                }

                { ( outerObj.type == "image") && 
                    <div>
                        { (outerObj.value != "") &&
                            <img className="preview" style={{width: "200px"}} src={"/content/blocks/" + bid + "/" + buid + "/" + outerObj.id + "/" + outerObj.value} />
                        }
                        <br />
                        <FileInput />
                    </div>
                }
                 
            </div>
        )
    }
}


class Block extends React.Component
{
    constructor(props) {
        super(props);

        this.state = {
            block: this.props.data || []
        }
    }

    render(){
        var id = this.state.block.id,
            uid = this.state.block.uid,
            title = this.state.block.title,
            order = this.props.order || 1,
            groupList = this.props.groupList,
            isFresh = this.state.block.freshBlock;
        // console.log("render " + id + "_" + uid)
        // console.log(this.state.block.variables);
        var variables = this.state.block.variables.map(function(item, index){
            if (groupList[item.type] == undefined) {
                return (
                    <Variable key={item.id} data={item} bid={id} buid={uid}/>
                )
            } else {
                return (
                     <Group key={item.id} data={item} bid={id} buid={uid} groupList={groupList[item.type]} />
                )
            }
        })
 // console.log(variables);

        return (
            <div className="Block">
                <DeleteElement eKey={id + "_" + uid} isFresh={isFresh} />
                <input type="hidden" className="block_id_holder" value={id} />
                <input type="hidden" className="block_unique_id_holder" value={uid} />
                <span className="block_header">{title}</span>
                <span>Порядковый номер блока</span>
                <TextInput additionalClasses="order_value" val={order}/>
                {variables}
            </div>
        )
    }
}


class BlockRegion extends React.Component
{
    constructor(props) {
        super(props);

        this.state = {
            blocks: this.props.data || []
        }
    }



    componentDidMount(){
        var self = this;

        window.ee.addListener('Block.delete', function(item) {
            var nextBlocks = self.state.blocks;
            var delIndex = undefined;
            nextBlocks.forEach(function(el, index){
                if ( (el.id + "_" + el.uid) == item.eKey ) {
                    delIndex = index;
                }
            });
            nextBlocks.splice(delIndex, 1);
            if (!item.isFresh) {
                quiet_ajax();
                $.ajax({
                    url: '/admin/constructor/pages/deleteAssignedBlock/' + $(".pageId").val()+ "_" + item.eKey,
                    type: 'POST'
                })
                .done(function (data) {
                    // console.log("success");
                    // cl(data);
                    // location.refresh();
                })
                .fail(function (data) {
                    console.log("error");
                });
            }

            self.setState({blocks: nextBlocks});
        });

        window.ee.addListener('Block.add', function(item) {

            var nextBlocks = self.state.blocks;

            var max = 0;
            nextBlocks.forEach(function(el, index){
                if (el.id == item.blockId) max++;
            })
            var new_buid = ++max;
            quiet_ajax();
            $.ajax({
                url: '/admin/constructor/ajax/blocks/get',
                type: 'POST',
                data: {
                    block_id: item.blockId
                },
            })
            .done(function(data) {//<-поправить возвращаемую дату
                data = JSON.parse(data);
                data.uid = new_buid;
                data.freshBlock = true;
                nextBlocks = [...nextBlocks, data];
                self.setState({blocks: nextBlocks});
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("blocks/get ajax complete");
            });

            
           
        });




    }

    componentWillUnmount() {
      window.ee.removeListener('Block.delete');
      window.ee.removeListener('Block.add');
    }

    render(){
        var groupList = this.props.groupList;
        cl(this.state.blocks);
        var blocks = this.state.blocks.map(function(item, index){
            return (
                <Block key={item.id + "_" + item.uid} order={item.order} data={item} groupList={groupList} />
            )
        });

        return(
            <div className="BlockRegion">
                {blocks}
            </div>
        )
    }
}


class WorkArea extends React.Component
{

    constructor(props) {
        super(props);

       this.cntr = 1;
       this.fd = null;
    }

    onElementAddButtonClick(e){
        window.ee.emit("Block.add", {
            blockId: e.value
        })
    }

    gatherData(context, component){
        var baseContext = this;
        var $this = $(context);
        var variables = [],
            groups = [],
            container = [],
            bruteListVariables = $this.children(".Variable"),
            bruteListGroups = $this.children(".Group"); 

        var parseValue = function(index){
            var $this = $(this),
                self = this,
                value = "",
                type = $this.children(".var_type").val();

            if (type == "text" || type == "code") {
                value = $this.find("textarea").val();
            } else if ( type == "wysiwyg") {
                value = escapeHtml(CKEDITOR.instances[$this.find("textarea").attr("name")].getData());
            } else if (type == "image" || type == "file") {
                // cl($this.find(".FileInput input"));
                var preview = $this.find(".preview");
                if ($this.find(".FileInput input")[0] != null && $this.find(".FileInput input")[0].files[0] != undefined) {
                    var uniqueName = component.cntr + $this.find(".FileInput input")[0].files[0].name; 
                    value = uniqueName;
                    component.fd.append(uniqueName, $this.find(".FileInput input")[0].files[0]);
                } else if (preview[0] != undefined && preview[0] != null) {
                    cl(preview);
                    var src = preview.eq(0).attr("src");
                    value = src.slice(src.lastIndexOf("/") + 1);
                } else {
                    value = null;
                }
            } else if (type == "repeater") {
                var repeaterData = []; 
                $this.children(".Repeater").children(".Line").each(function(index){
                    repeaterData[index] = baseContext.gatherData(this, component);
                })
                value = repeaterData;
            }

            container[index] = {
                id: $this.find(".var_id").val(),
                type: $this.find(".var_type").val(),
                value: value
            }

        }

        if (bruteListVariables.length > 0) {
            bruteListVariables.each(parseValue);
        }
        variables = container;
       var container = [];

        if (bruteListGroups.length > 0) {
            bruteListGroups.each(function(index) {
                var bruteListGroupsVarables = $(this).children(".Variable");
                if (bruteListGroupsVarables.length > 0) {
                    bruteListGroupsVarables.each(parseValue);
                }
                groups[index] = {
                    id: $(this).find(".var_id").val(),
                    type: "group",
                    group: container
                }
            });
        }

        return [...variables, ...groups];

    }


    onSubmitButtonClick(act){

        this.fd =  new FormData();
        var baseContext = this;
        var blocks = [];
        $(".Block").each(function(index) {
            var $this = $(this);
            var variables = baseContext.gatherData(this, baseContext);

            if (blocks[$this.find(".block_id_holder").val()] == undefined) {
                blocks[$this.find(".block_id_holder").val()] = [];
            }
            blocks[$this.find(".block_id_holder").val()][$this.find(".block_unique_id_holder").val()] = {
                order: $this.find(".order_value").val(),
                variables: variables

            }
        });

        var obj = {
            page_title: $(".page_title").val(), 
            page_url: $(".page_url").val(), 
            seo_title: $(".seo_title").val(), 
            seo_description: $(".seo_description").val(), 
            seo_h1: $(".seo_h1").val(), 
            header_text: $(".header_text").val(), 
            header_title: $(".header_title").val(), 
            header_content: $(".header_content").val(), 
            show_button: $(".show_button").val(), 
            header_class: $(".header_class").val(), 
            blocks: blocks
        }

        if (act == "update") {
            obj.pageId = this.props.data.page_id;
            if ($(".header_background")[0].files[0] != undefined) {
                var uniqueName = 1 + $(".header_background")[0].files[0].name;
                obj.header_background = uniqueName;
                this.fd.append(uniqueName, $(".header_background")[0].files[0]);
            } else {
                obj.header_background = null;
            }

        }
        this.fd.append('data', JSON.stringify(obj));
        console.log(obj);

        $.ajax({
            url: '/admin/constructor/pages/' + act,
            type: 'POST',
            processData: false,
            contentType: false,
            data: this.fd,
        })
        .done(function(data) {
            console.log("success");
            console.log(data);
            // location.refresh();
        })
        .fail(function(data) {
            console.log("error");
            console.log(data);
        })
        .always(function() {
            console.log("complete");
        });
        
    }


    render(){
        var data = this.props.data;
        var pageElements = data.base_params;


        return (
            <div className="WorkArea white_back">
                <div className="m_orders_titles">
                    <div className="news_zag"><a href="/admin/constructor/pages/view">Конструктор страниц</a></div>
                </div>
                <div className="adm_table">
                    <input type="hidden" className="pageId" value={data.page_id} />

                    <div className="admin-input__label">*Название страницы</div>
                    <TextInput val={pageElements.title} additionalClasses="page_title" />

                    <div className="admin-input__label">*Адрес страницы(начинается с "/")</div>
                    <TextInput val={pageElements.url} additionalClasses="page_url" />

                    <div className="admin-input__label">SEO title</div>
                    <TextInput val={pageElements.seo_title} additionalClasses="seo_title"  />

                    <div className="admin-input__label">SEO description</div>
                    <TextInput val={pageElements.seo_description} additionalClasses="seo_description" />

                    <div className="admin-input__label">SEO h1</div>
                    <TextInput val={pageElements.seo_h1} additionalClasses="seo_h1" />

                    <div className="admin-input__label">Текст в хедере</div>
                    <TextInput val={pageElements.header_text} additionalClasses="header_text" />

                    <div className="admin-input__label">Текст кнопки в хедере</div>
                    <TextInput val={pageElements.header_title} additionalClasses="header_title" />

                    <div className="admin-input__label">Содержимое хедера(параллакс)</div>
                    <Area val={htmlDecode(pageElements.header_content || "")} additionalClasses="header_content" />

                    <div className="admin-input__label">Показывать кнопку в хедере</div>
                    <CheckBox checked={pageElements.show_button} additionalClasses="show_button" />

                    <div className="admin-input__label">ID хедера</div>
                    <TextInput val={pageElements.header_class} additionalClasses="header_class" />

                    { (data.page_id != undefined) && 
                        <div>
                            <div className="admin-input__label">Задний фон хедера</div>
                            { (pageElements.header_background != "") &&
                                <img style={{width: "200px"}} src={"/content/pages/" + data.page_id + "/" + pageElements.header_background} />
                            }
                            <br />
                            <FileInput additionalClasses="header_background" />
                        </div>
                    }

                    <div className="delimiter"></div>

                    <BlockRegion data={data.blocks} groupList={data.types_content}/>

                    <RefSelecter optData={data.avaliable_blocks} link={el => this.link = el} />
                    <RedButton text={"Добавить блок"}  callback={(link) => this.onElementAddButtonClick(this.link)}/>

                    {   (data.page_id == undefined) &&
                        <RedButton text={"Сохранить"} callback={(act) => this.onSubmitButtonClick("save")} />
                    }

                    {   (data.page_id != undefined) &&
                        <RedButton text={"Обновить"} callback={(act) => this.onSubmitButtonClick("update")} />
                    }

                </div>
            </div>
        )
    }
}


ReactDOM.render(
    <WorkArea data={backData} />,
    document.getElementById('root')
);