window.ee = new EventEmitter();
var groupCounter = 0;

class TypeSelector extends React.Component 
{
	constructor(props) {
		super(props);
        this.state = {
             value: this.props.val
         }
    }

	change(event){
		this.setState({value: event.target.value});
	}

	render(){
		var dataTS = this.props.dataTS;
		return (
			<div className="TypeSelector">
				<label for="block-type" className="admin-input__label">Тип переменной</label>
				<select id="block-type" className="admin-input__label" name="block_type" onChange={(e) => this.change(e)} value={this.state.value}>
					<option value="" >Выберите тип</option>
					<option value="repeater">Повторитель</option>
					<option value="text">Текст</option>
					<option value="wysiwyg">Редактор</option>
					<option value="code">Код</option>
					<option value="image">Изображение</option>
					<option value="file">Файл</option>
				</select>
				{this.state.value == "repeater" ? <Group dataG={dataTS.group}/> : ""}
			</div>
		)
	}
};


class Title extends React.Component
{
	constructor(props) {
		super(props);
        this.state = {
			textValue: this.props.val || ''
		}
	}

 	onChangeHandler(e) {
    	this.setState({textValue: e.target.value})
 	}

	render(){
		var name = this.props.name;
		return (
			<div className="Title">
				<label for="block-name" className="admin-input__label">{name}</label>
				<input 
					id="block-name" 
					className="admin-input"
					type="text" 
					name="block_title" 
					onChange={(e) => this.onChangeHandler(e)}
					value={this.state.textValue}
				/>
			</div>
		)
	}
};

class DeleteElement extends React.Component
{	
	constructor(props) {
		super(props);
		//this.onBtnClickHandler = this.onBtnClickHandler.bind(this);
	}

	onBtnClickHandler(){

		window.ee.emit("Element.delete." + this.props.guid, {
			elementKey: this.props.eKey
		});
	}

	render(){
		return (
			<div className="DeleteElement">
				<button onClick={() => this.onBtnClickHandler()} >X</button>
			</div>
		)
	}
};

class Element extends React.Component
{
	constructor(props) {
		super(props);
        this.state = {
           data: this.props.dataE || {id: 0, title:"", type: "", data: {}}
        }
    }

	render(){
		var data = this.state.data;
			return (
				<div className="Element">
			        <Title name={'Имя переменной'} val={data.title}/>
			        <TypeSelector val={data.type} dataTS={data.data}/>
			        <DeleteElement guid={this.props.guid} eKey={data.id}/>
			    </div>
			)

	}
};

class ButtonAdd extends React.Component
{
	render(){
		var cont = this.props.cont;
		return (
			<div className="ButtonAdd">
				 <button href="#" className="red_bt add-variable__btn" hidefocus="true" onClick={() => this.props.setCont([...cont,{
				 	id: cont.length,
            		title: "",
            		type: "",
            		data: ""
   				 }])} >+</button>
		    </div>
		)
	}
};

class Group extends React.Component
{
	constructor(props) {
		super(props);
        this.state = {
           cont: this.props.dataG || [{id: 0, title:"", type: "", data: {}}],
           uid: ++groupCounter
        }
    }

    componentDidMount() {

	  var self = this;
	  console.log();
	  window.ee.addListener('Element.delete.' + self.state.uid, function(item) {
	  	var nextCont = self.state.cont;
	  	var delIndex = undefined;
	  	nextCont.forEach(function(el, index){
	  		if (el.id == item.elementKey) {
	  			delIndex = index;
	  		}
	  	});
	  	nextCont.splice(delIndex, 1);
	    self.setState({cont: nextCont, uid: self.state.uid});
	  });
	}

	componentWillUnmount() {
	  var self = this;
	  window.ee.removeListener('Element.delete.' + self.state.uid);
	  --groupCounter;
	}

	render(){
		var self = this;
		var Elements = self.state.cont.map(function(item, index){
			return (
				  <Element key={item.id} dataE={item} guid={self.state.uid} />
			)
		});
		return (
			<div className="Group">
		        {Elements}
		        <ButtonAdd cont={this.state.cont} setCont={c => self.setState({cont: c})}/>
		    </div>
		)
	}
};


class WorkArea extends React.Component
{
  onSaveBtnClick(){

  	var getGroupByRecursive = function(rootNode){

  		var result = [];
  		if (rootNode.length == 0) {
  			return result;
  		}
  		rootNode.children(".Element").each(function(index, el) {
  			el = $(el);

  			var newGroup = getGroupByRecursive(el.find(".Group").eq(0));

  			if (newGroup.length > 0) {
  				newGroup = {group:newGroup}
  			} else {
  				newGroup = {};
  			}
  			result.push({
  				id: index, 
  				title: el.find(".Title input").val(), 
  				type: el.find(".TypeSelector select").val(), 
  				data: newGroup
  			})
  		});

  		return result;
  	};

  	var object = {
  		id: typeIndex,
  		title: $(".WorkArea > .Title input").eq(0).val(),
  		uniqueTitle: $(".WorkArea > .Title input").eq(1).val(),
  		group: JSON.stringify(getGroupByRecursive($(".WorkArea > .Group")))
  	};



  	console.log(object);

  	$.ajax({
  		url: '/admin/constructor/types/save',
  		type: 'POST',
  		data: object,
  	})
  	.done(function() {
  		location = "/admin/constructor/types/";
  	})
  	.fail(function() {
  		alert("Ошибка сохранения");
  	});
  	
  	//window.ee.emit("Area.save.1", {item:0});
  }

  render() {
    return (
      <div className="WorkArea white_back">

     	<div className="m_orders_titles">
			<div className="news_zag"><a href="/admin/constructor/blocks/view">Конструктор переменных</a></div>
		</div>

		<Title name={'Название группы'} val={data.title} />

		<Title name={'ключ группы'} val={data.uniqueTitle} />

        <Group dataG = {data.group} />

        <button  className="red_bt variable-save" hidefocus="true" onClick={this.onSaveBtnClick}>Сохранить</button>

      </div>
    );
  }
};

ReactDOM.render(
  <WorkArea />,
  document.getElementById('root')
);